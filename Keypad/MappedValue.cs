﻿namespace Keypad
{
    public class MappedValue
    {
        public char Group { get; set; }
        public string Value { get; set; }
    }
}