﻿namespace Keypad
{
    public class TransformContainer
    {
        public TransformContainer(string source, string result)
        {
            Source = source;
            Result = result;
        }

        public string Source { get; set; }
        public string Result { get; set; }
    }
}