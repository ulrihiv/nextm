﻿using System.Collections.Generic;
using System.Linq;
using Keypad.Interfaces;

namespace Keypad.Impl
{
    public class KeypadGenerator : IKeypadGenerator
    {
        private readonly IKeypadMapper _mapper;
        private readonly ITransformer _transformer;

        public KeypadGenerator(IKeypadMapper mapper, ITransformer transformer)
        {
            _mapper = mapper;
            _transformer = transformer;
        }

        public List<TransformContainer> Generate(List<string> input)
        {
            var map = _mapper.Map();
            return input.Select(inputString =>new TransformContainer(inputString, _transformer.Transform(inputString, map))).ToList();
        }
    }
}