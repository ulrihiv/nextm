﻿using System;
using System.Collections.Generic;
using Keypad.Interfaces;

namespace Keypad.Impl
{
    public class StringTranformer : ITransformer
    {
        public string Transform(string inputString, Dictionary<char, MappedValue> map)
        {
            var previousGroup = new char();
            var result = "";

            foreach (var character in inputString)
            {
                if (map.ContainsKey(character))
                {
                    if (map[character].Group == previousGroup)
                    {
                        result += " ";
                    }

                    previousGroup = map[character].Group;
                    result += map[character].Value;
                }
                else
                {
                    throw new ArgumentException($"invalid string: {inputString}");
                }
            }

            return result;
        }
    }
}