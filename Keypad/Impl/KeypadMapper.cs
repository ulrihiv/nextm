﻿using System.Collections.Generic;
using Keypad.Interfaces;

namespace Keypad.Impl
{
    public class KeypadMapper : IKeypadMapper
    {
        private readonly Dictionary<char, char[]> _input;

        public KeypadMapper()
        {
            _input = new Dictionary<char, char[]>
            {
                {'1', new[] {'a', 'b', 'c'}},
                {'2', new[] {'d', 'e', 'f'}},
                {'3', new[] {'g', 'h', 'i'}},
                {'4', new[] {'j', 'k', 'l'}},
                {'5', new[] {'m', 'n', 'o'}},
                {'6', new[] {'p', 'q', 'r', 's'}},
                {'8', new[] {'t', 'u', 'v'}},
                {'9', new[] {'w', 'x', 'y', 'z'}},
                {'0', new[] {' '}},
            };
        }

        public Dictionary<char, MappedValue> Map()
        {
            var output = new Dictionary<char, MappedValue>();
            foreach (var item in _input)
            {
                for (var i = 0; i < item.Value.Length; i++)
                {
                    output.Add(item.Value[i], new MappedValue()
                    {
                        Value = new string(item.Key, i + 1),
                        Group = item.Key
                    });
                }
            }

            return output;
        }
    }
}