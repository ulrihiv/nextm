﻿using System.Collections.Generic;

namespace Keypad.Interfaces
{
    public interface IKeypadGenerator
    {
        List<TransformContainer> Generate(List<string> input);
    }
}