﻿using System.Collections.Generic;

namespace Keypad.Interfaces
{
    public interface ITransformer
    {
        string Transform(string inputString, Dictionary<char, MappedValue> map);
    }
}