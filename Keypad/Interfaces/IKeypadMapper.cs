﻿using System.Collections.Generic;

namespace Keypad.Interfaces
{
    public interface IKeypadMapper
    {
        Dictionary<char, MappedValue> Map();
    }
}