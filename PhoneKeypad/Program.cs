﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Keypad.Impl;

namespace PhoneKeypad
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputFileName = ConfigurationManager.AppSettings["input"];
            var generator = new KeypadGenerator(new KeypadMapper(), new StringTranformer());

            if (string.IsNullOrWhiteSpace(inputFileName))
                Console.WriteLine("Please add source file (source.txt)");

            try
            {
                var inputSource = File.ReadAllLines(inputFileName);
                var result = generator.Generate(inputSource.ToList());

                for (var i = 0; i < result.Count; i++)
                {
                    Console.WriteLine($"Case #{i+1}: {result[i].Result}");
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Please add source file (source.txt)");
            }

            Console.Read();
        }
    }
}
