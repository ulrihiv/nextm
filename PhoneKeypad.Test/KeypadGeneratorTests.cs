﻿using System;
using System.Collections.Generic;
using Keypad.Impl;
using NUnit.Framework;

namespace PhoneKeypad.Test
{
    [TestFixture]
    public class KeypadGeneratorTests
    {
        [Test]
        public void ThrowsExceptionIfStringInvalid()
        {
            var generator = new KeypadGenerator(new KeypadMapper(), new StringTranformer());
            var input = new List<string>() {"1111"};

            Assert.Throws<ArgumentException>(() => { generator.Generate(input); });
        }

        [Test]
        public void GeneratesCorrectOutput()
        {
            var generator = new KeypadGenerator(new KeypadMapper(), new StringTranformer());
            var input = new List<string>() { "abc" };

            var result = generator.Generate(input);

            Assert.AreEqual("1 11 111", result[0].Result);
        }

        [Test]
        public void GeneratesCorrectOutputForDuplicatedSymbols()
        {
            var generator = new KeypadGenerator(new KeypadMapper(), new StringTranformer());
            var input = new List<string>() { "aabbcc" };

            var result = generator.Generate(input);

            Assert.AreEqual("1 1 11 11 111 111", result[0].Result);
        }

        [Test]
        public void GeneratesCorrectOutputForDuplicatedSymbolsWithSpaces()
        {
            var generator = new KeypadGenerator(new KeypadMapper(), new StringTranformer());
            var input = new List<string>() { "aa bb cc" };

            var result = generator.Generate(input);

            Assert.AreEqual("1 1011 110111 111", result[0].Result);
        }

        [Test]
        public void GeneratesCorrectOutputForDuplicatedSpaces()
        {
            var generator = new KeypadGenerator(new KeypadMapper(), new StringTranformer());
            var input = new List<string>() { "  bb cc" };

            var result = generator.Generate(input);

            Assert.AreEqual("0 011 110111 111", result[0].Result);
        }
    }
}