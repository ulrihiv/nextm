﻿using System.Linq;
using Keypad.Impl;
using NUnit.Framework;

namespace PhoneKeypad.Test
{
    [TestFixture]
    public class KeypadMapperTests
    {
        [Test]
        public void KeypadMapperCreatesCorrectKeyMap()
        {
            var mapper = new KeypadMapper();
            var map = mapper.Map();

            var alphabet = "abcdefghijklmnopqrstuvwxyz ";

            for (var i = 0; i < map.Count; i++)
            {
                Assert.AreEqual(map.ElementAt(i).Key, alphabet[i]); 
            }
        }

        [Test]
        public void KeypadMapperCreatesCorrectValueMap()
        {
            var mapper = new KeypadMapper();
            var map = mapper.Map();

            Assert.AreEqual(map['a'].Value, "1");
            Assert.AreEqual(map['b'].Value, "11");
            Assert.AreEqual(map['c'].Value, "111");

            Assert.AreEqual(map['w'].Value, "9");
            Assert.AreEqual(map['x'].Value, "99");
            Assert.AreEqual(map['y'].Value, "999");
            Assert.AreEqual(map['z'].Value, "9999");
        }

    }
}